" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
  "nerdtree
  Plug 'scrooloose/nerdtree'
  "icon for nerdtree
  Plug 'ryanoasis/vim-devicons'
  "align text
  Plug 'godlygeek/tabular'
  "git
  Plug 'airblade/vim-gitgutter'
  Plug 'tpope/vim-fugitive'
  "syntax checking
  Plug 'vim-syntastic/syntastic'
  "airline
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  "completion
  Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
  Plug 'valloric/youcompleteme'
  "tag bar (add info on class)
  Plug 'majutsushi/tagbar'
  "color scheme
  Plug 'flazz/vim-colorschemes'
  Plug 'dylanaraps/wal.vim'
  Plug 'jeaye/color_coded', {
  \ 'build': {
    \   'unix': 'rm -f CMakeCache.txt && cmake . && make && make install',
  \ },
  \ 'autoload': { 'filetypes' : ['c', 'cpp', 'objc', 'objcpp'] },
  \ 'build_commands' : ['cmake', 'make']
\}
  "surrond text with <> '' ""
  Plug 'tpope/vim-surround'
  "comment code
  Plug 'scrooloose/nerdcommenter'
  "spinet
  Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
  "isolate text
  Plug 'chrisbra/NrrwRgn'
  "markdown & pandoc
  Plug 'plasticboy/vim-markdown'
  Plug 'vim-pandoc/vim-pandoc'
  Plug 'vim-pandoc/vim-pandoc-syntax' 
  "write table in markdown
  Plug 'dhruvasagar/vim-table-mode'
  "support bépo layout
  "if !empty(system("xkb-switch -p | grep bepo"))
  "  Plug 'michamos/vim-bepo'
  "endif
call plug#end()
set nocompatible
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-space>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
let g:ycm_show_diagnostics_ui = 0
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
set background=dark
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:chromatica#libclang_path='/usr/local/opt/llvm/lib'
let g:chromatica#enable_at_startup=1
let g:chromatica#responsive_mode=1
autocmd VimEnter * wincmd p
let NVIM_TUI_ENABLE_TRUE_COLOR=1

colorscheme wal
" aides visuelles
map <f8> :TagbarToggle<CR>
set number
set ruler
set showcmd

" recherche incrémentale
set hlsearch
set incsearch
if has('nvim')
  set inccommand=nosplit
endif

" auto-indentation à 2 espaces
set autoindent
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

" options honteuses (pour les faibles)
"set clipboard=unnamedplus
"set mouse=a

" un peu de cohérence dans un monde de brutes
nmap Y y$
nmap U <C-r>

filetype plugin on
let g:ycm_confirm_extra_conf=0
imap kj <Esc>
"let g:XkbSwitchLib="/usr/lib/libxkbswitch.so"

"pandoc
let g:pandoc#formatting#mode = "sA"
let g:pandoc#after#modules#enabled = ["formatting", "folding", "bibliographies", "completion", "metadata", "menu", "executors", "keyboard", "toc", "chdir", "spell", "hypertext", "nrrwrgn", "tablemode"]
let g:pandoc#completion#bib#mode = 'citeproc'
let g:pandoc#formatting#smart_autoformat_on_cursormoved = 1
let g:pandoc#syntax#codeblocks#embeds#langs = ["ruby", "literatehaskell=lhaskell", "bash=sh", "c", "cpp",  "lua", "java", "python", "nasm", "asm"]
