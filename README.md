# dotfiles
## Screenshot
![screenshot](screenshot.png)
## info
- Editor : nvim
- WM : awesome wm
- Distro : arch
- Terminal emulator : termite
- Shell : zsh (with grml zsh config)
- colors : wal
- icons : Archdroid (generated with oomox)
- gtk : Materia (oomox)
- wallpaper : VA-11 HALL-A
- fonts : nerd fonts

### awesome
- lain (https://github.com/lcpz/lain)
- tyranical (https://github.com/Elv13/tyrannical)

## nvim
- nerdtree
- tabular
- vim-markdown
- vim-gitgutter
- syntastic
- vim airlines
- youcompleteme
- vim wal
- etc.
