source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 
export ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)
export ZSH_HIGHLIGHT_STYLES[comment]=fg=white,bold
alias ls=exa
alias ip="ip --color"
alias ipb="ip --color --brief"
alias ll='exa -lhHg'
alias la='exa -lhHg'

alias ccat='pygmentize -g -O style=colorful,linenos=1'
bindkey '^ ' autosuggest-accept
